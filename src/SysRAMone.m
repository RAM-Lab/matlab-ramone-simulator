% To simulate RAMone

classdef SysRAMone
    
    properties
        
        q =      [0 0 0 0 0 0 0]'; % [x y phi alphaL betaL alphaR betaR]
        q_dot =  [0 0 0 0 0 0 0]';
        u =      [0 0 0 0 0 0 0]'; 
        t; % time
        W; % Work
        
        
        p;
        param;
        
        
        M;
        k;
        J_left;
        J_right;
        J;
    end
    
    
    methods
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Constructor
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function obj = SysRAMone(y_,p_)
            
            if nargin > 0
                y = y_;
            else
                y = ContStateDefinition;
            end
            obj = obj.SetState(y);
            
            
            if nargin > 1
                obj.p = p_;
            else
                obj.p = SystParamDefinition;
            end
            obj.param = obj.p(1:13);
        end

        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Getters
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function [contactPoints] = GetContactPoints(obj)
            q_ = num2cell(obj.q);
            param_ = num2cell(obj.param);
            
            lContact = ContactPointL(q_{:}, param_{:});
            rContact = ContactPointR(q_{:}, param_{:});
            
            contactPoints = [lContact, rContact];
        end
        
        function [state] = GetState(obj)
            state = [zeros([2*size(obj.q,1) 1]); obj.u(4:7); obj.t; obj.W];
            state(1:2:13) = obj.q;
            state(2:2:14) = obj.q_dot;
        end
        
        function [timeElapsed] = GetTimeElapsed(obj)
            timeElapsed = obj.t;
        end
                
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Setters
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function obj = SetState(obj, stateIn)
            
            y = stateIn;
            
            obj.q = y(1:2:13);
            obj.q_dot = y(2:2:14);
            obj.t = y(19);
            obj.W = y(20);
        end

        
        function obj = SetMotorVelocities(obj, motorVelocities)
        end

       
        function obj = SetBodyConstraints(obj, bodyConstraints)
        end
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Constraints and dynamics
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function obj = Update(obj)

            q_ = num2cell(obj.q);
            q_dot_ = num2cell(obj.q_dot);
            param_ = num2cell(obj.param);
            
            obj.M = MassMatrix(q_{:}, param_{:});
            obj.k = F_CoriGrav(q_{:}, q_dot_{:}, param_{:});
            obj.J_left  = ContactJacobianL(q_{:}, param_{:});
            obj.J_right = ContactJacobianR(q_{:}, param_{:});
            obj.J = [obj.J_left; obj.J_right];
        end
            
        
        % Returns a scalar value, if this value is less than 0 the pose
        % is not in the admissable region
        
        function y = G(obj, q, q_dot, lambda, contactIdx)
            assert(contactIdx < 3)
            
            if contactIdx == 1
                y = obj.G_left(q);
            else
                y = obj.G_right(q);
            end
        end
        
        function y = G_left(obj, q)
            
            q_ = num2cell(q);
            param_ = num2cell(obj.param);
            
            lContact = ContactPointL(q_{:}, param_{:});
            y = lContact(2);
        end
        
        function y = G_right(obj, q)
                        
            q_ = num2cell(q);
            param_ = num2cell(obj.param);
            
            rContact = ContactPointR(q_{:}, param_{:});
            y = rContact(2);
        end
        

        % Returns the tangential velocity of the particle
        function x_dot = V(obj, q, q_dot, lambda,  contactIdx)
            assert(contactIdx < 3)
            
            if contactIdx == 1
                x_dot = obj.V_left(q, q_dot);
            else
                x_dot = obj.V_right(q, q_dot);
            end
        end
        
        function x_dot = V_left(obj, q, q_dot)
            x_dot = obj.J_left(1,:)*q_dot;
        end
        
        function x_dot = V_right(obj, q, q_dot)
            x_dot = obj.J_right(1,:)*q_dot;
        end
    end
end