clear all; close all; clc;

nbrItrs = 10000;

% Setup
y = ContStateDefinition;
motorVelocities = 0;
bodyConstraints = 0;
Ts = 0.001;
RAMoneStep(y, motorVelocities, bodyConstraints, Ts);
tic
for i=1:nbrItrs
    RAMoneStep(y, motorVelocities, bodyConstraints, Ts);
end
runTime = toc;

timePerItr = runTime/nbrItrs

timePerItr/Ts

