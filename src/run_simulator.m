clear all; close all; clc;

% Behavior
isAnimate = false;
timeBeforeExit = 3;

% Setup
y = ContStateDefinition;
p = SystParamDefinition;


% Animation
if isAnimate
    itrBeforeRedraw = 3;
    drawObj = DrawRAMone(y,p);
end

% RAMoneStep inputs
motorVelocities = 0; % unimplemented
bodyConstraints = 0; % unimplemented
Ts = 1e-3;


itr = 0;
play=true;
while play

    
    [c, y] = RAMoneStep(y, motorVelocities, bodyConstraints, Ts);
    t = y(19);

    if isAnimate
        % Update display every `itrBeforeRedraw` iterations
        if itr == itrBeforeRedraw
            UpdateRAMone(drawObj,y,t);
            itr = 0;
        else
            itr = itr+1;
        end
    end

    if (t > timeBeforeExit)
        play = false;
        close all
    end
end