function [ system ] = Controller(system, t_)
%CONTROLLER 

    if nargin > 1
        t = t_;
    else
        t = system.GetTimeElapsed();
    end

    % Create persistant variable and once defined, assign to true. 
    % This fireOnce will be assigned false after it fires
    persistent fireOnce;
    if isempty(fireOnce)
        fireOnce = true;
    end

    input = zeros(size(system.u)); % create default value for input
    if 0
        if t > .2 && fireOnce
            fireOnce = false;
            fprintf('Fire!\n');
            input(1:2) = [-5000; 30000];
            if size(input,1) == 3
                input(3) = 0;
            end
        end
    end
    
    system.u = input;
end

