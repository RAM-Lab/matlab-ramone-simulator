
function [patchData] = TransformPatch(patchData, transVec)
            theta = -transVec(3);
            rotMat = [cos(theta), -sin(theta); sin(theta), cos(theta)];
            shiftMat = repmat(transVec(1:2)',size(patchData,1),1);
            
            patchData = patchData*rotMat + shiftMat;
end