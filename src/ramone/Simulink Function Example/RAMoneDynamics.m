function [jointTorquesOut,contactOut,stateOut] = RAMoneDynamics(motorVelocities, bodyConstraints, stateIn)

Ts = 1e-3;

coder.extrinsic('RAMoneStep')

contactOut = zeros(2,1);
stateOut = zeros(20,1);

[c_, y_] = RAMoneStep(stateIn,motorVelocities,bodyConstraints,Ts);

contactOut(:) = c_(:);
stateOut(:) = y_(:);

jointTorquesOut = zeros(4,1);

end