
%% Definitions
% Generalized coordinates
syms x y phi alphaL betaL alphaR betaR
q    = [x y phi alphaL betaL alphaR betaR];
% Generalized speeds
syms dx dy dphi dalphaL dbetaL dalphaR dbetaR
dqdt = [dx dy dphi dalphaL dbetaL dalphaR dbetaR];


% Define the necessary parameter subset:
% Gravity
syms g
% Segment dimensions (assume the left and right part are identical):
syms l2 l3 lL1 lL2 lH rFoot
% Masses/Inertia;
syms m1 m2 m3
syms j1 j2 j3

%param = [l2 l3 lL1 lL2 rFoot g m1 m2 m3 j1 j2 j3];
param = [g m1 m2 m3 lL1 lL2 lH l2 l3 rFoot j1 j2 j3];

% syms strideFreq sinalpha cosalpha sinbeta cosbeta
% s = [strideFreq sinalpha cosalpha sinbeta cosbeta];

%% DYNAMICS (obtained via the Euler-Lagrange equation)

% CoG-orientations (from kinematics):
CoG_MB_ang     = phi;
CoG_ThighL_ang = phi+alphaL;
CoG_ShankL_ang = phi+alphaL+betaL;
CoG_ThighR_ang = phi+alphaR;
CoG_ShankR_ang = phi+alphaR+betaR;
% CoG-positions (from kinematics):
CoG_MB    = [x;
             y];
% lL1(lR1) and lL2(lR2) are the total length of the thigh and shank for
% left (right) part
CoG_ThighL = [x + lH * sin(CoG_MB_ang) + l2 * sin(CoG_ThighL_ang);
              y - lH * cos(CoG_MB_ang) - l2 * cos(CoG_ThighL_ang)];
CoG_ShankL  = [x + lH * sin(CoG_MB_ang) + lL1 * sin(CoG_ThighL_ang) + (lL2-l3) * sin(CoG_ShankL_ang);
               y - lH * cos(CoG_MB_ang) - lL1 * cos(CoG_ThighL_ang) - (lL2-l3) * cos(CoG_ShankL_ang)]; 
CoG_ThighR = [x + lH * sin(CoG_MB_ang) + l2 * sin(CoG_ThighR_ang);
              y - lH * cos(CoG_MB_ang) - l2 * cos(CoG_ThighR_ang)];
CoG_ShankR  = [x + lH * sin(CoG_MB_ang) + lL1 * sin(CoG_ThighR_ang) + (lL2-l3) * sin(CoG_ShankR_ang);
               y - lH * cos(CoG_MB_ang) - lL1 * cos(CoG_ThighR_ang) - (lL2-l3) * cos(CoG_ShankR_ang)];
         
% CoG-velocities (computed via jacobians):
d_CoG_MB         = jacobian(CoG_MB,q)*dqdt.';
d_CoG_ThighL     = jacobian(CoG_ThighL,q)*dqdt.';
d_CoG_ShankL     = jacobian(CoG_ShankL,q)*dqdt.';
d_CoG_ThighR     = jacobian(CoG_ThighR,q)*dqdt.';
d_CoG_ShankR     = jacobian(CoG_ShankR,q)*dqdt.';
d_CoG_MB_ang     = jacobian(CoG_MB_ang,q)*dqdt.';
d_CoG_ThighL_ang = jacobian(CoG_ThighL_ang,q)*dqdt.';
d_CoG_ShankL_ang = jacobian(CoG_ShankL_ang,q)*dqdt.';
d_CoG_ThighR_ang = jacobian(CoG_ThighR_ang,q)*dqdt.';
d_CoG_ShankR_ang = jacobian(CoG_ShankR_ang,q)*dqdt.';

% Potential Energy (due to gravity):
V = CoG_MB(2)*m1*g + CoG_ThighL(2)*m2*g + CoG_ShankL(2)*m3*g + CoG_ThighR(2)*m2*g + CoG_ShankR(2)*m3*g;
V = prod(simplify(factor(V)));

% Kinetic Energy:         
T = 0.5 * (m1 * sum(d_CoG_MB.^2) + ...
           m2 * sum(d_CoG_ThighL.^2) + ...
           m3 * sum(d_CoG_ShankL.^2) + ...
           m2 * sum(d_CoG_ThighR.^2) + ...
           m3 * sum(d_CoG_ShankR.^2) + ...
           j1 * d_CoG_MB_ang^2 + ...
           j2 * d_CoG_ThighL_ang^2 + ...
           j3 * d_CoG_ShankL_ang^2 + ...
           j2 * d_CoG_ThighR_ang^2 + ...
           j3 * d_CoG_ShankR_ang^2);
T = prod(simplify(factor(T)));

% Lagrangian:
L = T-V;
% Partial derivatives:
dLdq   = jacobian(L,q).';
dLdqdt = jacobian(L,dqdt).';
      
% Compute Mass Matrix:
M = jacobian(dLdqdt,dqdt);
M = simplify(M);

% Compute the coriolis and gravitational forces:
dL_dqdt_dt = jacobian(dLdqdt,q)*dqdt.';
f_cg = dLdq - dL_dqdt_dt;
f_cg = simplify(f_cg);

% The equations of motion are given with these functions as:   
% M * dqddt = f_cg(q, dqdt) + u;

%% KINEMATICS (for collision detection, computation, and graphical output)
% Joint-positions:
MB = [x;
      y];

Hip  = MB + [+lH * sin(CoG_MB_ang);
             -lH * cos(CoG_MB_ang)];
    
KneeL = Hip + [+lL1 * sin(CoG_ThighL_ang);
               -lL1 * cos(CoG_ThighL_ang)];
KneeR = Hip + [+lL1 * sin(CoG_ThighR_ang);
               -lL1 * cos(CoG_ThighR_ang)];
     
FootL = KneeL + [+lL2 * sin(CoG_ShankL_ang);
                 -lL2 * cos(CoG_ShankL_ang)];
FootR = KneeR + [+lL2 * sin(CoG_ShankR_ang);
                 -lL2 * cos(CoG_ShankR_ang)];
                
% Contact points:
cont_pointR = FootR + [+0;
                       -rFoot]
cont_pointL = FootL + [+0;
                       -rFoot]

% Contact Jacobian:
% NOTE: this is a non-holonomic contact constraint.  I.e. the Jacobian
% needs to be expanded afterwards with the foot rotation:
JR = jacobian(cont_pointR,q) + [0,0,rFoot,0,0,rFoot,rFoot;
                                0,0,0,    0,0,0,    0];
JL = jacobian(cont_pointL,q) + [0,0,rFoot,rFoot,rFoot,0,0;
                                0,0,0,    0,    0,    0,0];
JR = simplify(JR);
JL = simplify(JL);

% CoGs (incl orientation of the segments):
CoGs = [CoG_MB,     CoG_ThighL,     CoG_ShankL,     CoG_ThighR,     CoG_ShankR;
        CoG_MB_ang, CoG_ThighL_ang, CoG_ShankL_ang, CoG_ThighR_ang, CoG_ShankR_ang];

% Links (or rather: joint positions)
MB_Left   = MB + [+0.3 * cos(CoG_MB_ang);
                  +0.3 * sin(CoG_MB_ang)];
MB_Right  = MB + [-0.3 * cos(CoG_MB_ang);
                  -0.3 * sin(CoG_MB_ang)];
links = [FootR, KneeR, Hip, MB, MB_Left, MB_Right, MB, Hip, KneeL, FootL];

% Position of the foot points:     
footPts = [FootL, FootR];

%% Create MATLAB-functions:
if ~exist('AutoGeneratedFcts','dir')
    mkdir('AutoGeneratedFcts')
end

matlabFunction(M,'file','AutoGeneratedFcts/MassMatrix','vars',[q, param]);
matlabFunction(f_cg,'file','AutoGeneratedFcts/F_CoriGrav','vars',[q, dqdt, param]);

matlabFunction(cont_pointR,'file','AutoGeneratedFcts/ContactPointR','vars',[q, param]);
matlabFunction(cont_pointL,'file','AutoGeneratedFcts/ContactPointL','vars',[q, param]);
matlabFunction(JR,'file','AutoGeneratedFcts/ContactJacobianR','vars',[q, param]);
matlabFunction(JL,'file','AutoGeneratedFcts/ContactJacobianL','vars',[q, param]);
matlabFunction(CoGs,'file','AutoGeneratedFcts/CoGPositions','vars',[q, param]);
matlabFunction(links,'file','AutoGeneratedFcts/LinkPositions','vars',[q, param]);
matlabFunction(footPts,'file','AutoGeneratedFcts/FootPtPositions','vars',[q, param]);