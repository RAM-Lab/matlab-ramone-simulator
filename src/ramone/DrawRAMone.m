function [obj] = DrawRAMone(y,p)
    bodyColor = 0.8*ones(1,3);
   
    thighColor = 0.5*ones(1,3);
    shankColor = 0.5*ones(1,3);
    hipColor = 0.5*ones(1,3);
    kneeColor = shankColor;
    backLegShading = 0.5;
    footColor = 'b';

    % Set up object parameters:
    obj.rFoot    = p(10); % Save the foot radius for later use
    obj.p        = p;     % Save the parameter vector for later use
    obj.yRange   = [-0.1 0.9];  %Set a hard upper and Lower Limit
    obj.xWidth   = 16/9*(obj.yRange(2) - obj.yRange(1));   %This give standard HD aspect Ratio
    obj.xTickWidth   = 0.2;
    obj.textScale = 1.5;

    % Get geometry data from file
    geom = load('RAMoneGeometry.mat');
    obj.bodyPoints = geom.body_points2;
    obj.thighPoints = geom.thigh_points;
    obj.shankPoints = geom.shank_points;
    obj.rHip = geom.hip_radius;
    obj.rKnee = geom.knee_radius;

    % Set up output figure:
    obj.fig = figure;
    obj.ax = axes;
   
    box on
    grid off
    axis equal
    hold on

    set(obj.fig,'Name','2D-Output using a simplified link-based representation');
    set(obj.fig,'Color','w');
    % Get initial values, which determine how many links, CoGs,
    % etc. we will draw:
    [CoGs, links] = GraphicalKinematicsWrapper(y, p);
    % Create the ground. The shaded area reaches from +-5. It has
    % to be shifted in the plot routine if the quadruped is moving.
    % Only the Baseline is 'infinitely' long
    line([-10000000,10000000],[0,0],'LineWidth',3,'Color',[0,0,0]);
    % The ground:
    obj.xdata_ground=[];
    obj.ydata_ground=[];
    for i = -15:0.15:15
        obj.xdata_ground = [obj.xdata_ground,[i-0.15;i+0.15]];
        obj.ydata_ground = [obj.ydata_ground,[-0.3;0]];
    end
    obj.ground_graph = line(obj.xdata_ground,obj.ydata_ground,'Color',[0,0,0]);

    % The patches and joints
    body = TransformPatch(obj.bodyPoints,CoGs(:,1));
    thighL = TransformPatch(obj.thighPoints,CoGs(:,2));
    shankL = TransformPatch(obj.shankPoints,CoGs(:,3));
    thighR = TransformPatch(obj.thighPoints,CoGs(:,4));
    shankR = TransformPatch(obj.shankPoints,CoGs(:,5));

    obj.shankLPatch = patch(shankL(:,1),shankL(:,2),'k','edgecolor','k','facecolor',shankColor*backLegShading);
    obj.kneeLCirc = rectangle('position',obj.rKnee*[-1 -1 2 2] + [links(:,end-1)', 0, 0],'curvature',[1 1],'edgecolor','k','facecolor',kneeColor*backLegShading);
    obj.footLCirc = rectangle('position',obj.rFoot*[-1 -1 2 2] + [links(:,end)', 0, 0],'curvature',[1 1],'edgecolor','k','facecolor',footColor);
    obj.thighLPatch = patch(thighL(:,1),thighL(:,2),'k','edgecolor','k','facecolor',thighColor*backLegShading);
    obj.bodyPatch = patch(body(:,1),body(:,2),'k','edgecolor','k','facecolor',bodyColor);
    obj.shankRPatch = patch(shankR(:,1),shankR(:,2),'k','edgecolor','k','facecolor',shankColor);
    obj.kneeRCirc = rectangle('position',obj.rKnee*[-1 -1 2 2] + [links(:,2)', 0, 0],'curvature',[1 1],'edgecolor','k','facecolor',kneeColor);
    obj.footRCirc = rectangle('position',obj.rFoot*[-1 -1 2 2] + [links(:,1)', 0, 0],'curvature',[1 1],'edgecolor','k','facecolor',footColor);
    obj.thighRPatch = patch(thighR(:,1),thighR(:,2),'k','edgecolor','k','facecolor',thighColor);
    obj.hipRCirc = rectangle('position',obj.rHip*[-1 -1 2 2] + [links(:,3)', 0, 0],'curvature',[1 1],'edgecolor','k','facecolor',hipColor);

    % Crop plot to reasonable dimensions
    axis(obj.ax,[CoGs(1,1) - obj.xWidth/2, CoGs(1,1) + obj.xWidth/2, obj.yRange(1), obj.yRange(2)]);

%             set(obj.ax,'xgrid','off','ygrid','off','xtick',[],'ytick',[])
    obj.xTickWidth = obj.ax.XTick(2) - obj.ax.XTick(1);
%             obj.ax.FontSize = obj.ax.FontSize*obj.textScale;


    % Display the simulation time
    obj.hText = text(CoGs(1,1) - 0.40*obj.xWidth,0.55,sprintf('Time: %6.3fs',0));
    set(obj.hText, 'Units', 'normalized');
    set(obj.hText, 'Position', [0.1, 0.85]);
end

