% function [systParamVec, systParamNames, systParamIndices] = SystParamDefinition()
% function p = SystParamDefinition()
%
% This MATLAB function defines the physical system parameter vector 'p' for
% a trotting biped in 2D.  Besides serving as initial configuration of the
% model, this file provides a definition of the individual components of
% the system parameter vector and an index struct that allows name-based 
% access to its values.
%
% NOTE: This function is relatively slow and should not be executed within
%       the simulation loop.
%
% Input:  - NONEl
% Output: - The initial system parameters as the vector 'systParamVec' (or 'p')
%         - The corresponding parameter names in the cell array 'systParamNames' 
%         - The struct 'systParamIndices' that maps these names into indices  
%
% Created by C. David Remy on 04/09/2011
% MATLAB 2010a
%
% Documentation:
%  'A MATLAB Framework For Gait Creation', 2011, C. David Remy (1), Keith
%  Buffinton (2), and Roland Siegwart (1),  International Conference on
%  Intelligent Robots and Systems, September 25-30, San Francisco, USA 
%
% (1) Autonomous Systems Lab, Institute of Robotics and Intelligent Systems, 
%     Swiss Federal Institute of Technology (ETHZ) 
%     Tannenstr. 3 / CLA-E-32.1
%     8092 Zurich, Switzerland  
%     cremy@ethz.ch; rsiegwart@ethz.ch
%
% (2) Department of Mechanical Engineering, 
%     Bucknell University
%     701 Moore Avenue
%     Lewisburg, PA-17837, USA
%     buffintk@bucknell.edu
%
%   See also HYBRIDDYNAMICS, FLOWMAP, JUMPMAP, JUMPSET, 
%            CONTSTATEDEFINITION, DISCSTATEDEFINITION, EXCTSTATEDEFINITION,
%            EXCTPARAMDEFINITION, 
%            VEC2STRUCT, STRUCT2VEC. 
%
function [systParamVec, systParamNames, systParamIndices] = SystParamDefinition()
    
    % All units are (m,kg,s)
    
    % Physics:
    systParam.g         = 9.81;             % [m/s^2] gravity
%     systParam.g         = 0;              % [g] zero-gravity

    systParam.m1        = 7.9026;             % [kg] mass of the main body
    systParam.m2        = 0.7887;             % [kg] mass of the upper leg segments
    systParam.m3        = 0.569;             % [kg] mass of the lower leg segments
    systParam.lL1       = 0.2;              % [m] length of the upper leg segments
    systParam.lL2       = 0.2639;           % **Unverified**[m] length of the lower leg segments
    systParam.lH        = 0.137675;         % [m] Distance from the hip axis to pitch axis
    systParam.l2        = 0.0503;            % [m] distance between hip joints and CoG of the upper leg segments
    systParam.l3        = 0.2639 - 0.0927;    % [m] distance between foot points and CoG of the lower leg segments
    systParam.rFoot     = 0.0563/2;         % [m] foot radius
    systParam.j1        = 0.04;             % [kg*m^2] inertia of the main body
    systParam.j2        = 0.002207;           % [kg*m^2] inertia of the upper leg segments
    systParam.j3        = 0.006518;           % [kg*m^2] inertia of the lower leg segments
    systParam.kalpha_R  = 74;               % **Unverified**[N*m/rad] rotational spring stiffness in the hip joints
    systParam.kalpha_L  = 74;               % **Unverified**[N*m/rad] rotational spring stiffness in the hip joints
    systParam.balpha    = 7;%0.7;              % **Unverified**[N*m/(rad/s^2)] damping constant  Use 20% of critical damping
    systParam.kbeta1    = 80;               % **Unverified**[N*m/rad] rotational spring stiffness in the knee joints at small deflections 
    systParam.bbeta1    = 10;%3;                % **Unverified**[N*m/(rad/s^2)] damping constant at small angles with negative velocity
    systParam.kbeta2_R  = 32;               % **Unverified**[N*m/rad] rotational spring stiffness in the knee joints at large deflections
    systParam.kbeta2_L  = 30;               % **Unverified**[N*m/rad] rotational spring stiffness in the knee joints at large deflections
    systParam.bbeta2    = 1;%0.1;              % **Unverified**[N*m/(rad/s^2)] damping constant at large angles or small angles with positive velocity
    systParam.beta_sm_R = 0.0424*systParam.kbeta2_R/(systParam.kbeta1 - systParam.kbeta2_R);             % **Unverified**[rad] boundary between small and large angles
    systParam.beta_sm_L = 0.0276*systParam.kbeta2_L/(systParam.kbeta1 - systParam.kbeta2_L);             % **Unverified**[rad] boundary between small and large angles
    
    systParam.muS       = 0.9;             % **Unverified**[1] static coefficient of friction on the foot
    systParam.muK       = 0.8;              % **Unverified**[1] kinetic coefficient of friction on the foot
    
    systParam.kFootSpring = 5e4;
    systParam.bFootSpring = 5e3;
    systParam.sigmaFootSpring = 1e-4;
    
    timeConst           = 0.001;
    systParam.kphi      = systParam.j1*(2*pi/timeConst)^2;
    systParam.bphi      = 2*sqrt(systParam.kphi*systParam.j1);
    systParam.bx        = systParam.m1/timeConst;
    
    
    [systParamVec, systParamNames] = Struct2Vec(systParam);
    systParamIndices = Vec2Struct(1:1:length(systParamVec),systParamNames);
end
