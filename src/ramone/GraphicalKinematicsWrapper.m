% function [CoGs, links, footPts] = GraphicalKinematicsWrapper(y, p)
%
% This MATLAB function calls the automatically generated function code that
% computes the positions of all CoGs, links, and the foot points, to
% facilitate graphical output.  This function is not called directly to
% reflect the definition of the continuous states 'y'.  It is written for
% the model of a bounding quadruped. 
%
% Input:  - A vector of continuous states 'y'
%         - A vector of model system parameters 'p'
% Output: - The position and orientation (in rows of [x;y;ang]) of all CoGs
%         - A series of line segments, that represent the links of the
%           robot
%         - All foot points of the robot.
%
%
% Created by C. David Remy on 03/14/2011
% MATLAB 2010a 
%
% Documentation:
%  'A MATLAB Framework For Gait Creation', 2011, C. David Remy (1), Keith
%  Buffinton (2), and Roland Siegwart (1),  International Conference on
%  Intelligent Robots and Systems, September 25-30, San Francisco, USA 
%
% (1) Autonomous Systems Lab, Institute of Robotics and Intelligent Systems, 
%     Swiss Federal Institute of Technology (ETHZ) 
%     Tannenstr. 3 / CLA-E-32.1
%     8092 Zurich, Switzerland  
%     cremy@ethz.ch; rsiegwart@ethz.ch
%
% (2) Department of Mechanical Engineering, 
%     Bucknell University
%     701 Moore Avenue
%     Lewisburg, PA-17837, USA
%     buffintk@bucknell.edu
%
%   See also SYMBOLICCOMPUTATIONOFEQM, CONTSTATEDEFINITION,
%            SYSTPARAMDEFINITION. 

function [CoGs, links, footPts] = GraphicalKinematicsWrapper(y, p)
%     % Map the generalized coordinates:
%     % Keep the index-structs in memory to speed up processing
%      persistent contStateIndices
%      if isempty(contStateIndices)
%          [~, ~, contStateIndices] = ContStateDefinition();
%      end
     x       = y(1);
     y_      = y(3);
     phi     = y(5);
     alphaL  = y(7);
     betaL   = y(9);
     alphaR  = y(11);
     betaR   = y(13);

%     x       = y(01);
%     y_      = y(03);
%     phi     = y(05);
%     alphaL  = y(07);
%     betaL   = y(09);
%     alphaR  = y(11);
%     betaR   = y(13);
    
%     % Map the system paramters:
%     % Keep the index-structs in memory to speed up processing
%      persistent systParamIndices
%      if isempty(systParamIndices)
%          [~, ~, systParamIndices] = SystParamDefinition();
%      end
     l2    = p(8);
     l3    = p(9);
     rFoot = p(10);
     g     = p(1);
     m1    = p(2);
     m2    = p(3);
     m3    = p(4);
     j1    = p(11);
     j2    = p(12);
     j3    = p(13);
     lL1   = p(5);
     lL2   = p(6);
     lH    = p(7);

%     l2    = p(07);
%     l3    = p(08);
%     rFoot = p(09);
%     g     = p(01);
%     m1    = p(04);
%     m2    = p(05);
%     m3    = p(06);
%     j1    = p(10);
%     j2    = p(11);
%     j3    = p(12);
    
    % Call the auto-generated functions
    CoGs    = CoGPositions(x,y_,phi,alphaL,betaL,alphaR,betaR,g,m1,m2,m3,lL1,lL2,lH,l2,l3,rFoot,j1,j2,j3);
	links   = LinkPositions(x,y_,phi,alphaL,betaL,alphaR,betaR,g,m1,m2,m3,lL1,lL2,lH,l2,l3,rFoot,j1,j2,j3);
	footPts = FootPtPositions(x,y_,phi,alphaL,betaL,alphaR,betaR,g,m1,m2,m3,lL1,lL2,lH,l2,l3,rFoot,j1,j2,j3);
end
