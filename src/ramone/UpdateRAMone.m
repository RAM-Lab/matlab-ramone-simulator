function obj = UpdateRAMone(obj, y, t)
    % Plot the current state of the system by updating the lines
    % and patches that have been saved before 

    % Get an updated computation of the kinematics:
    [CoGs, links] = GraphicalKinematicsWrapper(y, obj.p);

    body = TransformPatch(obj.bodyPoints,CoGs(:,1));
    thighL = TransformPatch(obj.thighPoints,CoGs(:,2));
    shankL = TransformPatch(obj.shankPoints,CoGs(:,3));
    thighR = TransformPatch(obj.thighPoints,CoGs(:,4));
    shankR = TransformPatch(obj.shankPoints,CoGs(:,5));

    set(obj.shankLPatch,'XData',shankL(:,1),'YData',shankL(:,2));
    set(obj.kneeLCirc,'Position',obj.rKnee*[-1 -1 2 2] + [links(:,end-1)', 0, 0]);
    set(obj.footLCirc,'Position',obj.rFoot*[-1 -1 2 2] + [links(:,end)', 0, 0]);
    set(obj.thighLPatch,'XData',thighL(:,1),'YData',thighL(:,2));
    set(obj.bodyPatch,'XData',body(:,1),'YData',body(:,2));
    set(obj.shankRPatch,'XData',shankR(:,1),'YData',shankR(:,2));
    set(obj.kneeRCirc,'Position',obj.rKnee*[-1 -1 2 2] + [links(:,2)', 0, 0]);
    set(obj.footRCirc,'Position',obj.rFoot*[-1 -1 2 2] + [links(:,1)', 0, 0]);
    set(obj.thighRPatch,'XData',thighR(:,1),'YData',thighR(:,2));
    set(obj.hipRCirc,'Position',obj.rHip*[-1 -1 2 2] + [links(:,3)', 0, 0]);


    % Shift the ground to the next integer next to the center of
    % the model:
%             for m = 1:length(obj.xdata_ground)
%                set(obj.ground_graph(m),'XData',obj.xdata_ground(:,m)+round(CoGs(1,1)));
%             end
    % The axis is set, such that it overlaps all CoGs by about 2:

    axis(obj.ax,[CoGs(1,1) - obj.xWidth/2, CoGs(1,1) + obj.xWidth/2, obj.yRange(1), obj.yRange(2)]);  
    obj.ax.XTick = (ceil(obj.ax.XLim(1)/obj.xTickWidth):1:floor(obj.ax.XLim(2)/obj.xTickWidth))*obj.xTickWidth;
%             obj.hText = text(CoGs(1,1) - 0.40*obj.xWidth,ceil(max(CoGs(2,:))+0.5)-0.2,['Time: ',num2str(0)]);
    if nargin > 2
        set(obj.hText,'String',sprintf('Time: %6.3f s',t));
    end
%             set(obj.hText,'Position',[floor(min(CoGs(1,:))-0.5)+0.2,ceil(max(CoGs(2,:))+0.5)-0.2,0],'String',['Time: ',num2str(t)]);
    drawnow();

end