function [contactPoints, stateOut] = RAMoneStep(stateIn, motorVelocities, bodyConstraints, Ts)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Getters
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function contactPoints = GetContactPoints(sys)
        contactPoints = sys.GetContactPoints();
    end

    function stateOut = GetState(sys)
        stateOut = sys.GetState();
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Setters
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function sys = SetState(sys, stateIn)
        sys = sys.SetState(stateIn);
    end

    function sys = SetMotorVelocities(sys, motorVelocities)
        sys = sys.SetMotorVelocities(motorVelocities);
    end

    function sys = SetBodyConstraints(sys, bodyConstraints)
        sys = sys.SetBodyConstraints(bodyConstraints);
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    function sys = NextTimestep(sys)
    %NEXTTIMESTEP updates a systems pose and velocity by one time step. To
    %do this it first solves for the contact forces, which is the zero
    %vector for any contacts that are in the air, then it uses the solved
    %for contact forces to update the pose and velocity at the next
    %time-step.
    %
    % Note that ContactSolver first tries to solve the contact forces
    % iteratively. If iterating results in the system checking the same
    % configuration twice, the ContactSolver switches to a brute force
    % mode, where it evaluates all contact modes to see if they satisfy the
    % contraints. If no mode does, the ContactSolver finds which mode was
    % recommended most when evaluating all possible modes. This mode is
    % then set to be the contact configuration.
        
        lambda = ContactSolver(); 
        [sys.q, sys.q_dot] = F(lambda);
        
        sys.t = sys.t + Ts;
    end

    function lambda = ContactSolver()
    %CONTACTSOLVER solves for the contact forces at each contact point. To
    %do this, it iterates through different modes and evaluates their
    %corresponding constraints. If the constraints are all satisfied, the
    %contact forces solved for are returned. If the ContactSolver
    %identifies that it is trying a mode for the second time, e.g.,
    %{'flight', 'flight'},then it switches to a brute force constraint 
    %evaluator. 
    %
    % The brute force constraint solver checks each possible mode for a
    % given number of contacts. Each mode is evaluated to see if it
    % satisfies the constraints. If a mode does satisfy all constraints,
    % then the configuration and contact forces are returned. Otherwise, if
    % there is no mode that satisfies the constraints, the system picks
    % whichever mode was recommended most while evaluating all possible
    % constraints. This works through counting the number repeated modes
    % that have been recommended by the function CheckAndRecommendMode.

        lambda = zeros([size(sys.J,1) 1]);

        % Initialize prevModes with zeros so that it can be appeneded to.
        % Also, this allows comparisson to see if has been checked already
        prevModes = zeros([1 nbrContacts]);
        isNotSatisfied = true;
        while isNotSatisfied

            cMode = cellfun(modeStrToNbr,contactModes);
            if isFirstTimeModeIsChecked(cMode, prevModes)
                
                prevModes = [prevModes; cMode];
                
                lambda = Solve(contactModes);
                [isNotSatisfied, contactModes] = CheckAndRecommendMode(contactModes, lambda);
            else
                [lambda, contactModes] = BruteContactSolver();
            end
        end
    end

    function [lambda] = Solve(contactModes)
    %Solve solves for the contact forces given the modes for all contacts.
    %If fsolve returns flag not equal to 1 the function returns an error.
            [result,~,flag] = fsolve( GenerateConstraintsFunc(contactModes),...
                                      zeros([size(sys.J,1) 1]), opts);
            lambda = 1/Ts*result;
            if flag ~= 1
                warning('fsolve return flag %i\n', flag);
            end
    end


    %%%% Functions for the simulated system
    function sys = UpdateSys(sys)
    %UPDATESYS updates the system's Jacobian matrix, Corelois vector, and 
    %Mass matrixfor the system.
    
        sys = sys.Update();
    end

    function [q_plus,q_dot_plus] = F(lambda)
    %F solves for a system's next generalized position and velocity given
    %contact forces. It also takes into account the input to the system.
    
        q_ddot = sys.M\(sys.k+sys.J'*lambda+sys.u);
        q_dot_plus = sys.q_dot + Ts*q_ddot;
        q_plus = sys.q + Ts*q_dot_plus;
    end


    %%%% Evaluate and select contact modes
    function [isNotSatisfied, newContactModes] = CheckAndRecommendMode(contactModes, lambda)
    %CHECKANDRECOMMENDMODE checks if a given contact force and contact mode
    %satisfy the system's constraints. It does this by creating a
    %vector that is the length of the number of contacts. Then, each
    %contact is evaluated to see if it satisfies the constraints that
    %associate with its current mode. If a constraint is not satisfied,
    %this leads the system to suggest a new contact mode. This algorithm is
    %then looped to converge to the current mode in the ContactSolver. In
    %the BruteForceModeEvaluator, the return newContactModes is used to
    %vote towards a contact mode if no mode satisfies the constraints. 
        
        constraintEval = zeros([1 nbrContacts]);
        
        [q_plus, q_dot_plus] = F(lambda);
        for contactIdx = 1:nbrContacts

            mode = contactModes{contactIdx};
            lambda_x = lambda(1 + 2*(contactIdx-1));
            lambda_y = lambda(2 + 2*(contactIdx-1));

            switch mode

                case 'flight'
                    if sys.G(q_plus, q_dot_plus, lambda, contactIdx) < 0                            
                        mode = 'ground';
                    else
                        constraintEval(contactIdx) = true;
                    end

                case 'ground'
                    if lambda_y < 0
                        mode = 'flight';
                    elseif lambda_x > mu * lambda_y
                        mode = 'slide left';
                    elseif lambda_x < -mu * lambda_y
                        mode = 'slide right';
                    else
                        constraintEval(contactIdx) = true;
                    end

                case 'slide left'
                    if lambda_y < 0
                        mode = 'flight';
                    elseif sys.V(q_plus, q_dot_plus, lambda, contactIdx) > 0
                        mode = 'ground';
                    else
                        constraintEval(contactIdx) = true;
                    end

                case 'slide right'
                    if lambda_y < 0
                        mode = 'flight';
                    elseif sys.V(q_plus, q_dot_plus, lambda, contactIdx) < 0
                        mode = 'ground';
                    else
                        constraintEval(contactIdx) = true;
                    end

                otherwise
                    error('ERROR: mode does not exist');
            end

            contactModes{contactIdx} = mode; 
        end
        
        isNotSatisfied = any(~constraintEval);
        newContactModes = contactModes;
    end

    function [isNotRepeat] = isFirstTimeModeIsChecked(cMode, prevModes)
    %ISFIRSTTIMEMODEISCHECKED returns false if any row in the array
    %prevModes is equal to the current contact mode, cModes. 
        isNotRepeat = ~any(ismember(prevModes, cMode,'rows'));
    end

    function [newLambda, newContactModes] = BruteContactSolver()
    %BRUTECONTACTSOLVER is run if the iterative solver repeats in
    %suggesting the same set of contact modes. BruteContactSolver
    %works by finding all possible combinations of contact modes for a
    %given number of contact points. These modes are evaluated until one of
    %them satisfies the constraints or all modes are evaluated. If all
    %modes are evaluated the function votes on which contact mode to
    %choose. When each possible combination is evaluated, it's recommended
    %mode is recorded. The list of all recommended modes is then used to
    %count which mode was recommended most often. The mode with the highest
    %number of recommendations is choosen to be the mode to proceed with.
    %Once the mode is choosen, the contact forces are then solved for.

        lambda = zeros([size(sys.J,1) 1]);
    
        % Generate all possible modes, do a set difference of previous
        % modes and possible modes
        possibleModesNbr = CombinationsWithReplacement(modeNbrs,nbrContacts);
        possibleModes = cellfun(modeNbrToStr, num2cell(possibleModesNbr));
        nbrPossibleModes = size(possibleModes, 1);

        idx = 1;
        modeGuesses = zeros(size(possibleModes));
        isNotSatisfied = true;
        while isNotSatisfied && idx <= nbrPossibleModes 

            contactModes = possibleModes(idx,:);
            lambda = Solve(contactModes);
            
            [isNotSatisfied, modeGuessStr] = CheckAndRecommendMode(contactModes, lambda);
            
            modeGuess = cellfun(modeStrToNbr,modeGuessStr);
            modeGuesses(idx,:) = modeGuess;
            idx = idx+1;
        end
        
        % If still not satisified, use vote to determine which is the next
        % mode. If the vote is a tie, the next mode is randomly selected. 
        if isNotSatisfied
            setRows = size(possibleModesNbr,1);
            
            score = zeros([setRows 1]);
            for row = 1:setRows
                score(row) = sum(ismember(modeGuesses, possibleModesNbr(row,:),'rows'));
            end
            
            [~,idx] = max(score);
            newContactModes = cellfun(modeNbrToStr, num2cell(possibleModesNbr(idx,:)));
            newLambda = Solve(contactModes);
        else
            newContactModes = contactModes; 
            newLambda = lambda;
        end  
    end


    %%%% Generate and evaluate constraints for various contact modes
    function constraints = GenerateConstraintsFunc(contactModes)
    %GENERATECONSTRAINTSFUNC applies a set of constraints given
    %contactModes. These constraints are then used to generate a function
    %that is in terms of lambda.
    %
    %To generate constraints, this function iterates over each contact
    %point and, from its mode, applies the corresponding constraints. 
        
        constraints = {};
        
        for contactIdx = 1:size(sys.J,1)/2
            mode = contactModes{contactIdx};

            switch mode

                case 'flight'
                    constraints = AppendConstraints(constraints, {'air'}, contactIdx);

                case 'ground'
                    constraints = AppendConstraints(constraints, {'g=0', 'v=0'}, contactIdx);

                case 'slide left'
                    constraints = AppendConstraints(constraints, {'g=0', 'lambda_x = mu lambda_y'}, contactIdx);

                case 'slide right'
                    constraints = AppendConstraints(constraints, {'g=0', 'lambda_x = -1 mu lambda_y'}, contactIdx);

                otherwise
                    error('ERROR: mode does not exist');
            end
        end
                    
        constraints = @(lambda) 1/Ts*EvaluateConstraintsInTermsOfLambda(constraints, 1/Ts*lambda);
    end

    function [constraints] = AppendConstraints(currentConstraints, constraintsToAdd, contactIdx)
    %APPENDCONSTRAINTS appends constraints to the current set of
    %constraints for a given contactIdx. For each constraint, a function
    %and function gradient, with respect to lambda, is given. 
    %
    %This funciton works through iterating over a cell array of constraints
    %given for a specific contactIdx. The cell array contains strings that
    %describe the desired constraints. This function significantly removes
    %code duplication. 
        
        constraints = currentConstraints;
        for i = 1:size(constraintsToAdd,2)

            constraintToAdd = constraintsToAdd{i};
            
            switch constraintToAdd
                case 'air'
                    func = @(q, q_dot, lambda, contactIdx) [lambda(1+2*(contactIdx-1)); lambda(2+2*(contactIdx-1))];
                case 'g=0'
                    func = @sys.G;
                case 'v=0'
                    func = @sys.V;
                case 'lambda_x = mu lambda_y' % slide left
                    func = @(q, q_dot, lambda, contactIdx) ...
                        mu * lambda(2+2*(contactIdx-1)) - lambda(1+2*(contactIdx-1));
                case 'lambda_x = -1 mu lambda_y' % slide right
                    func = @(q, q_dot, lambda, contactIdx) ...
                        lambda(1+2*(contactIdx-1)) + mu * lambda(2+2*(contactIdx-1));
                otherwise
                    error('ERROR: contstraintType does not exist');
            end

            grad = [];
            constraints = [constraints ... % dots are required for horzcat
                            {func;
                             grad;
                             contactIdx}];
         end
    end

    function [eqns, grads] = EvaluateConstraintsInTermsOfLambda(constraints, lambda)
    %EVALUATECONSTRAINTSINTERMSOFLAMBDA takes a cell vector of constraints
    %and uses those constraints to create a function that can be evaluated
    %given a value for lambda. This function is used in optimization to
    %solve for the contact forces. 
    %
    %Each constraint must contain the following, and in this order:
    %   - A function of q, q_dot, lambda, and the contactIdx that returns a
    %     scalar value or a vector
    %   - A gradient that corresponds to the function. The gradient is the
    %     function when its derivative is taken with respect to lambda
    %   - A contact index, which denotes the index of the contact point. 
    %
    %In this function, for each constraint, the constraint is unpacked an
    %then formated as an equation and gradient. The result of the function
    %and the gradient are both appended to vector arrays. After iterating
    %through all contact points, these arrays are then returned from the 
    %function. 
    %
    %Note that the output format of this is what fsolve would like to use
    %the analytic derivatives to speed convergence. 
        
        [q_plus,q_dot_plus] = F(lambda);

        args = {q_plus, q_dot_plus, lambda};
   
        eqns = [];
        grads = [];
        for i = 1:size(constraints,2)
            %Unpack constraints
            func = constraints{1,i};
            grad = constraints{2,i};
            contactIdx = constraints{3,i};
              
            % Append eqns and grads, which will be returned
            % Note function evaluates the function
            eqns = [eqns; func(args{:},contactIdx) ]; 
            grads = [grads; grad];
        end
    end


    %%%% Auxillary functions
    function combinations = CombinationsWithReplacement(set, nbr)
    %COMBINATIONSWITHREPLACEMENT takes a set and a number of repeats. It
    %then generates all combinations for the number of repeats for a set.
    %For example, if the set is {'0', '1'} and nbr is 2, the function would
    %generate {0 0; 0 1; 1 0; 1 1}.
    %
    %Note that this function works with cells and arrays. 

        m = length(set);
        X = cell(1, nbr);
        [X{:}] = ndgrid(set);
        X = X(end : -1 : 1); 
        y = cat(nbr+1, X{:});
        combinations = reshape(y, [m^nbr, nbr]);

    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Initialization
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Keep RAMone system in memory after funcall returns
    persistent sys opts mu nbrContacts modes contactModes modeNbrs ...
        modeStrToNbr modeNbrToStr
    
    if isempty(sys)
        sys = SysRAMone();
        sys = UpdateSys(sys);
        
        isAnalytic = false;
        if isAnalytic
            opts = optimoptions('fmincon', 'Diagnostics','off', 'Display','off','Algorithm', 'levenberg-marquardt','CheckGradients',true);
        else
            opts = optimoptions('fsolve', 'Diagnostics','off', 'Display','off');
        end
        
        mu = 0.5;
        
        nbrContacts = size(sys.J,1)/2; 
        
        modes = {'ground', 'flight', 'slide left', 'slide right'}';

        % Create cell with default mode
        defaultMode = 'flight';
        contactModes = cell([1 nbrContacts]);
        [contactModes{:}] = deal(defaultMode);
        
        modeNbrs = 1:length(modes);
        modeHashTable = containers.Map(modes, modeNbrs);

        modeStrToNbr = @(modeStr) modeHashTable(modeStr);    
        modeNbrToStr = @(modeNbr) modes(modeNbr);
    end

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Implementation
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Set system from function inputs
    sys = SetState(sys, stateIn);
    sys = SetMotorVelocities(sys, motorVelocities); % Unimplemented
    sys = SetBodyConstraints(sys, bodyConstraints); % Unimplemented
    
    
    % Step the system on time step forward
    % Note: operations return sys to indicate side-effects
    sys = Controller(sys);
    sys = UpdateSys(sys);
    sys = NextTimestep(sys);
    
    
    % Get returns from updated system
    contactPoints = GetContactPoints(sys);
    stateOut = GetState(sys);
end