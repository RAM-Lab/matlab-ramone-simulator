README
======

This repository is for a walking simulator for the RAM Lab.

To execute the code, add the entire repository to the path (make sure to include src), then run `run_simulator` in `src`.

All code was developed in MATLAB R2016a.
